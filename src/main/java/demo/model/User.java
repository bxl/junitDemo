package demo.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author buxianglong
 * @date 2018/10/31
 **/
@Entity
@Table(name = "user")
public class User{
    @Id
    @GeneratedValue
    private Long id;
    @Column(updatable = false)
    private Date createDate;
    private Date modifyDate;

    private String name;
    private int age;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public Date getCreateDate(){
        return createDate;
    }

    public void setCreateDate(Date createDate){
        this.createDate = createDate;
    }

    public Date getModifyDate(){
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate){
        this.modifyDate = modifyDate;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getAge(){
        return age;
    }

    public void setAge(int age){
        this.age = age;
    }

    @Override
    public String toString(){
        return "User{" +
                "id=" + id +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
