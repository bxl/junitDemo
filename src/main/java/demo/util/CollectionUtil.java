package demo.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 集合工具类
 *
 * @author buxianglong
 * @date 2018/11/01
 **/
public class CollectionUtil{
    public static <T> boolean isEmpty(final Collection<T> collections){
        if(null == collections || collections.size() <= 0){
            return true;
        }
        return false;
    }

    public static <T> boolean isNotEmpty(final Collection<T> collections){
        return !isEmpty(collections);
    }

    public static <T> List<T> emptyWrap(final List<T> lists){
        if(null != lists){
            return lists;
        }
        return new ArrayList<T>();
    }
}
