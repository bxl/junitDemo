package demo.service;

import demo.dao.UserDao;
import demo.exception.BusinessException;
import demo.model.User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author buxianglong
 * @date 2018/10/31
 **/
@Service
public class UserService{
    private UserDao userDao;

    public UserService(UserDao userDao){
        this.userDao = userDao;
    }

    /**
     * 新增用户
     *
     * @param user
     */
    public void addUser(User user) throws BusinessException{
        if(StringUtils.isEmpty(user.getName())){
            throw new BusinessException("用户名不能为空");
        }
        userDao.save(user);
    }
}
