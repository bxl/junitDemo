package demo.service;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * 随机名字服务
 *
 * @author buxianglong
 * @date 2018/10/31
 **/
public class RandomNameService{
    /**
     * 随机一个名字
     *
     * @return 名字
     */
    public String randomNickName(){
        List<String> nickNames = Arrays.asList("土豆", "番茄", "黄瓜", "白菜", "芹菜");
        return nickNames.get(new Random().nextInt(4));
    }
}
