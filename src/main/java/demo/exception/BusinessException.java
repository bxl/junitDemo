package demo.exception;

/**
 * @author buxianglong
 * @date 2018/11/01
 **/
public class BusinessException extends Exception{
    public BusinessException(){
    }

    public BusinessException(String message){
        super(message);
    }
}
