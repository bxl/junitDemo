package demo.dao;

import demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author buxianglong
 * @date 2018/10/31
 **/
@Repository
public interface UserDao extends JpaRepository<User, Long>{
    User findByName(String name);
}
