package demo.json;

/**
 * @author buxianglong
 * @date 2018/11/01
 **/
public class BusinessResult{
    public BusinessResult(int resultCode, String resultMessage){
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }

    private int resultCode;
    private String resultMessage;

    public int getResultCode(){
        return resultCode;
    }

    public void setResultCode(int resultCode){
        this.resultCode = resultCode;
    }

    public String getResultMessage(){
        return resultMessage;
    }

    public void setResultMessage(String resultMessage){
        this.resultMessage = resultMessage;
    }
}
