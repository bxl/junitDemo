package demo.basic;

import demo.exception.BusinessException;

/**
 * 数学计算工具类
 *
 * @author buxianglong
 * @date 2018/10/31
 **/
public class MathUtil{
    /**
     * 除法
     *
     * @param a 分子
     * @param b 分母
     * @return 结果
     */
    public static int divide(int a, int b){
        return a / b;
    }

    /**
     * 除法
     *
     * @param a 分子
     * @param b 分母
     * @return 结果
     */
    public static int divide2(int a, int b) throws BusinessException{
        if(0 == b){
            throw new BusinessException("分母不能为0");
        }
        return a / b;
    }
}
