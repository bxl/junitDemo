package demo.controller;

import demo.exception.BusinessException;
import demo.json.BusinessResult;
import demo.model.User;
import demo.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author buxianglong
 * @date 2018/11/01
 **/
@RestController
public class UserController{
    private UserService userService;

    public UserController(UserService userService){
        this.userService = userService;
    }

    @RequestMapping(value = "/user/add", method = RequestMethod.POST)
    public BusinessResult addUser(User user){
        try{
            userService.addUser(user);
            return new BusinessResult(1, "成功");
        }catch(BusinessException be){
            return new BusinessResult(0, be.getMessage());
        }catch(Exception e){
            return new BusinessResult(-1, "系统异常，请稍后再试。");
        }
    }
}
