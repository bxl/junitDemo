package demo.service;

import demo.exception.BusinessException;
import demo.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author buxianglong
 * @date 2018/11/01
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest{
    @Autowired
    private UserService userService;

    @Test
    public void testAddUser(){
        User user = new User();
        user.setName("");
        user.setAge(33);
        try{
            userService.addUser(user);
            Assert.fail("BusinessException is expected.");
        }catch(BusinessException be){
            Assert.assertEquals("用户名不能为空", be.getMessage());
        }
    }
}
