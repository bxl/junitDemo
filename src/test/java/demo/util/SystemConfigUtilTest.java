package demo.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SystemConfigUtilTest {
    @Test
    public void getProperties() {
        assertEquals("你好", SystemConfigUtil.getProperties("a"));
        assertEquals("1234567890", SystemConfigUtil.getProperties("b"));
        assertEquals("http://www.xiaomalixing.com/", SystemConfigUtil.getProperties("c"));
        assertEquals("Jack;Daniel;judy White;Bob;grey;Cube", SystemConfigUtil.getProperties("d"));
        assertNull(SystemConfigUtil.getProperties("e"));

        assertEquals("-", SystemConfigUtil.getProperties("e", "-"));
        assertEquals("null", SystemConfigUtil.getProperties("e", "null"));
        assertEquals("xyz", SystemConfigUtil.getProperties("f", "xyz"));
    }
}
