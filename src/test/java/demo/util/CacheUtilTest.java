package demo.util;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CacheUtilTest {
    private static final String key = "xmlx_redis_test_001";
    private static final String value = UUID.randomUUID().toString();

    @Test
    public void set() {
        CacheUtil.set(key, value);
        assertEquals(value, CacheUtil.get(key));
        //修改
        String newValue = UUID.randomUUID().toString();
        CacheUtil.set(key, newValue);
        assertEquals(newValue, CacheUtil.get(key));
    }

    @Test
    public void get() {
        assertTrue(null == CacheUtil.get(UUID.randomUUID().toString()));
    }

    @Test
    public void hset() {
        String key = "students_3_1";
        Map<String, String> map = new HashMap<String, String>() {{
            put("name", "张三");
            put("score", "88");
            put("gender", "1");
        }};
        CacheUtil.hset(key, map);
        Map<String, String> queryMap = CacheUtil.hget(key);
        assertEquals("张三", queryMap.get("name"));
        assertEquals("88", queryMap.get("score"));
        assertEquals("1", queryMap.get("gender"));

        assertEquals("张三", CacheUtil.hget(key, "name"));
        assertEquals("88", CacheUtil.hget(key, "score"));
        assertEquals("1", CacheUtil.hget(key, "gender"));

        CacheUtil.hset(key, "age", "16");
        assertEquals("16", CacheUtil.hget(key, "age"));
        CacheUtil.hset(key, "weight", "80");
        assertEquals("80", CacheUtil.hget(key, "weight"));
    }

    @Test
    public void hget() {
        assertTrue(null == CacheUtil.hget(UUID.randomUUID().toString()));

        assertTrue(null == CacheUtil.hget(UUID.randomUUID().toString(), "name"));
        assertTrue(null == CacheUtil.hget(UUID.randomUUID().toString(), "score"));
        assertTrue(null == CacheUtil.hget(UUID.randomUUID().toString(), "gender"));
    }

    @Test
    public void expire() {
        CacheUtil.set(key, value);
        CacheUtil.expire(key, 1);
        assertEquals(value, CacheUtil.get(key));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(null, CacheUtil.get(key));
    }

    @Test
    public void expireAt() {
        CacheUtil.set(key, value);
        CacheUtil.expireAt(key, buildDateAfterSeconds(1));
        assertEquals(value, CacheUtil.get(key));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(null, CacheUtil.get(key));
    }

    private Date buildDateAfterSeconds(int seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, seconds);
        return calendar.getTime();
    }
}
