package demo.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

/**
 * @author buxianglong
 * @date 2018/11/01
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest{
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    @Rollback
    public void testAddUser1(){
        String url = "http://localhost:" + port + "/user/add?name=王五&age=56";
        String jsonStr = testRestTemplate.postForObject(url, null, String.class, new HashMap<String, Object>());
        System.out.println(jsonStr);
        String expectedJsonStr = "{\"resultCode\":1,\"resultMessage\":\"成功\"}";
        JSONAssert.assertEquals(expectedJsonStr, jsonStr, true);
    }

    @Test
    @Rollback
    public void testAddUser2(){
        String url = "http://localhost:" + port + "/user/add?name=&age=56";
        String jsonStr = testRestTemplate.postForObject(url, null, String.class, new HashMap<String, Object>());
        String expectedJsonStr = "{\"resultMessage\":\"用户名不能为空\",\"resultCode\":0}";
        JSONAssert.assertEquals(expectedJsonStr, jsonStr, true);
    }
}
