package demo.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserController2Test {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testAddUser1() throws Exception {
        this.mockMvc.perform(post("/user/add?name={0}&age={1}", "王五", "56"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{\"resultCode\":1,\"resultMessage\":\"成功\"}"));
    }
}
