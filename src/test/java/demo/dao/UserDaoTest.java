package demo.dao;

import demo.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author buxianglong
 * @date 2018/10/31
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDaoTest{

    @Autowired
    private UserDao userDao;

    @Test
    public void testSave(){
        String name = "张三";
        int age = 21;
        Date currentDate = new Date();
        User user = buildUser(name, age, currentDate);
        userDao.save(user);
        assertEquals(1, userDao.count());
        User userFromDB = userDao.findByName(name);
        assertNotNull(userFromDB);
        assertEquals(name, userFromDB.getName());
        assertEquals(age, userFromDB.getAge());
        assertEquals(currentDate, userFromDB.getCreateDate());
    }

    private User buildUser(String name, int age, Date currentDate){
        User user = new User();
        user.setCreateDate(new Date());
        user.setModifyDate(new Date());
        user.setName("张三");
        user.setAge(21);
        return user;
    }
}
